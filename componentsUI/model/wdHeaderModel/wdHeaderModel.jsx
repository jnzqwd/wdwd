import './wdHeaderModel.scss'
export default {
  install: (Vue, options) => {
    Vue.component('wdHeaderModel', {
      // eslint-disable-next-line vue/require-prop-types
      props: ['isRotate', 'data', 'user'],
      data() {
        return {}
      },
      methods: {
        onShowMenu() {
          this.$emit('update:isRotate', !this.isRotate)
        }
      },
      render() {
        return (
          <header class="wdHeaderModel">
            <div class="header-wrapper">
              <div class="logo">
                <i class="iconfont icon-wdwd" />
                <i
                  class={`showMenu iconfont icon-caidan ${
                    this.isRotate ? 'rotate' : ''
                  }`}
                  on-click={this.onShowMenu}
                />
              </div>
              <wd-nav
                data={this.data}
                user={this.user}
                on-updateUser={() => $emit('updateUser')}
                on-exit={() => $emit('exit')}
              />
            </div>
          </header>
        )
      }
    })
    Vue.component('wdNav', {
      props: {
        data: {
          type: Array,
          default: () => []
        },
        user: {
          type: Object,
          default: () => ({
            id: 0,
            name: '用户名'
          })
        }
      },
      methods: {
        updateUser() {
          this.$emit('updateUser', this.user)
        },
        exit() {
          this.$emit('exit', this.user)
        }
      },
      render() {
        const dataDoms = this.data.map(item => (
          <li>
            <router-link to={item.url} replace>
              {item.name}
            </router-link>
          </li>
        ))
        return (
          <nav>
            <ul>
              {/* <li>首页</li>
              <li>本框架源码</li> */}
              {dataDoms}
              <li>
                <el-dropdown trigger="click">
                  <span class="el-dropdown-link">
                    <avue-avatar style="color: #f56a00; background-color: #fde3cf">
                      U
                    </avue-avatar>
                    <span class="userName">{this.user.name}</span>
                  </span>
                  <el-dropdown-menu slot="dropdown">
                    <el-dropdown-item on-click={this.updateUser}>
                      修改信息
                    </el-dropdown-item>
                    <el-dropdown-item on-click={this.exit}>
                      退出
                    </el-dropdown-item>
                  </el-dropdown-menu>
                </el-dropdown>
              </li>
            </ul>
          </nav>
        )
      }
    })
  }
}
