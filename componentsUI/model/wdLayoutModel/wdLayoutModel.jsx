import './wdLayoutModel.scss'
export default {
  install: (Vue, options) => {
    Vue.component('wdLayoutModel', {
      props: {
        isCollapse: {
          type: Boolean,
          default: false
        },
        isHeaderShow: {
          type: Boolean,
          default: true
        },
        isMenuShow: {
          type: Boolean,
          default: true
        },
        user: {
          type: Object,
          default: () => ({
            id: 0,
            name: '在这里写用户'
          })
        },
        menus: {
          type: Array,
          default: () => [
            {
              name: '首页',
              url: '/',
              icon: 'el-icon-tickets'
            },
            {
              name: '表单',
              url: '/form',
              icon: 'el-icon-tickets'
            },
            {
              name: '表格',
              url: '/table',
              icon: 'el-icon-tickets'
            },
            {
              name: '地图',
              url: '/map',
              icon: 'el-icon-tickets'
            },
            {
              icon: 'el-icon-tickets',
              name: '导航一',
              children: [
                {
                  name: '测试',
                  url: '/test',
                  icon: 'el-icon-tickets'
                },
                {
                  icon: 'el-icon-tickets',
                  name: '导航一',
                  children: [
                    {
                      name: '测试',
                      url: '/test',
                      icon: 'el-icon-tickets'
                    }
                  ]
                }
              ]
            }
          ]
        }
      },
      render() {
        let headerModel
        let menuModel
        if (this.isHeaderShow) {
          headerModel = (
            <wd-header-model
              is-rotate={this.isCollapse}
              user={this.user}
              {...{ on: { 'update:isRotate': val => (this.isCollapse = val) } }}
            />
          )
        }
        if (this.isMenuShow) {
          menuModel = (
            <wd-menu-model is-collapse={this.isCollapse} menus={this.menus} />
          )
        }
        return (
          <div id="wdLayoutModel">
            {headerModel}
            <el-container>
              {menuModel}
              <div class={`contextPage ${this.isCollapse ? 'pagemax' : ''}`}>
                <el-scrollbar style="height: 100%">
                  {this.$slots.default}
                </el-scrollbar>
              </div>
            </el-container>
          </div>
        )
      }
    })
  }
}
