import wdHeaderModel from './model/wdHeaderModel/wdHeaderModel.jsx'
import wdMenuModel from './model/wdMenuModel/wdMenuModel.jsx'
import wdLayoutModel from './model/wdLayoutModel/wdLayoutModel.jsx'
export default {
  install:(Vue, options) => {
    Vue.use(wdHeaderModel);
    Vue.use(wdMenuModel);
    Vue.use(wdLayoutModel);
  }
}
