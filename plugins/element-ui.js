import Vue from 'vue'
import Element from 'element-ui/lib/element-ui.common'
import locale from 'element-ui/lib/locale/lang/en'
import WdPluginJsx from '../componentsUI/Index.jsx'
import WdPluginVue from '../components/index.js'
import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'
import VCharts from 'v-charts'
export default () => {
  Vue.use(Element, { locale })
  Vue.use(VCharts)
  Vue.use(Avue)
  Vue.use(WdPluginJsx, {})
  Vue.use(WdPluginVue)
}
