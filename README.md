# wd-vue
# v1.0
> 一个基于nuxt的框架。然后进行jsx和vue的合理组合的一款新型框架，集成elementui和avue的优良特性，最重要的就是实现了同一导入vue和jsx文件，不需要每个vue文件进行导入

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

